package com.example.cerimuseum.DetailsItem;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;

import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cerimuseum.DetailsItem.DetailsFragmentArgs;
import com.example.cerimuseum.DetailsItem.DetailsViewModel;
import com.example.cerimuseum.R;
import com.example.cerimuseum.listItem.ItemAdapter;

public class DetailsFragment extends Fragment {

    private DetailsViewModel viewModel;
    private TextView name, brand, timeFrame, description, year, technicalDetails, categories;
    private ImageView miniature;

    private ImagesAdapter adapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ActionBar mActionBar;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Initialiser mActionBar ici pour afficher la flèche de retour par la suite.
        mActionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailsViewModel.class);

        DetailsFragmentArgs args = DetailsFragmentArgs.fromBundle(getArguments());
        String itemID = args.getItemId();
        viewModel.setItem(itemID);
        viewModel.setAllImages(itemID);

        listenerSetup();
        observerSetup();

    }
    private void listenerSetup() {
        name = getView().findViewById(R.id.item_name);
        brand = getView().findViewById(R.id.item_brand);
        timeFrame = getView().findViewById(R.id.item_timeFrame);
        year = getView().findViewById(R.id.item_year);
        description = getView().findViewById(R.id.item_description);
        technicalDetails = getView().findViewById(R.id.item_technical_details);
        categories = getView().findViewById(R.id.item_category);
        miniature = getView().findViewById(R.id.miniature);

        Glide.with(miniature.getContext())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+ viewModel.getItem().getValue().getId() +"/thumbnail")
                .into(miniature);

        // Pour afficher la flèche de retour en arrière.
        mActionBar.setDisplayHomeAsUpEnabled(true);

        // Pour gérer la liste des images et leur description
        recyclerView = getView().findViewById(R.id.list_image_view);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ImagesAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setDetailsViewModel(viewModel);



    }

    private void observerSetup() {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {
                        if(item.getName() != null) {
                            /*String text = "Nom : ";
                            if(item.getName().equals("")) {
                                text = text.concat("Non renseigné");
                            }
                            else text = text.concat(item.getName());*/
                            name.setText(item.getName());
                        }
                        if(item.getBrand() != null) {
                            String realBrand = "Marque : ";
                            realBrand = realBrand.concat(item.getBrand());
                            brand.setText(realBrand);
                        }
                        else brand.setText("Marque : Non renseigné");
                        if(item.getTimeFrame() != null) {
                            String[] timeFrames = item.getTimeFrame().split("[;]");
                            String realTimeFrame = "Plage de temps : ";
                            if(timeFrames.length == 0) {
                                realTimeFrame = realTimeFrame.concat("Non renseigné");
                            }
                            else {
                                for(String time : timeFrames) {
                                    if(!time.equals(timeFrames[timeFrames.length - 1])){
                                        realTimeFrame = realTimeFrame.concat(time + " / ");
                                    }
                                    else realTimeFrame = realTimeFrame.concat(time);
                                }
                            }

                            timeFrame.setText(realTimeFrame);
                        }
                        if(item.getYear() != null && item.getYear() != 0) {
                            String text = "Année : ".concat(item.getYear().toString());
                            year.setText(text);
                        }
                        else year.setText("Année : Non renseigné");
                        if(item.getDescription() != null) {
                            String text = "Description : ".concat(item.getDescription());
                            description.setText(text);
                        }
                        else description.setText("Description : Non renseigné");
                        if(item.getTechnicalDetails() != null) {
                            String text = "Détails techniques : ".concat(item.getTechnicalDetails());
                            technicalDetails.setText(text);
                        }
                        else technicalDetails.setText("Détails techniques : Non renseigné");
                        if(item.getCategories() != null) {
                            String text = "Catégories : ".concat(item.getCategories());
                            categories.setText(text);
                        }
                        else categories.setText("Catégories : Non renseigné");
                    }
                });
        viewModel.getAllImages().observe(getViewLifecycleOwner(),
                image -> {
                    Log.d("TAG", "Get all images");
                    if(image != null && !image.get(0).equals("")) {
                        adapter.setListImage(image);
                    }
                    else recyclerView.setVisibility(View.GONE);
                });
    }
}