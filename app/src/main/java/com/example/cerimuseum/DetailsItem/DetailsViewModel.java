package com.example.cerimuseum.DetailsItem;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.cerimuseum.data.MuseumRepository;
import com.example.cerimuseum.data.item.Item;

import java.util.List;

public class DetailsViewModel extends AndroidViewModel {
    private final MuseumRepository repo;
    private MutableLiveData<Item> item;
    private MutableLiveData<List<String>> allImages;

    public DetailsViewModel(@NonNull Application application) {
        super(application);
        repo = MuseumRepository.get(application);
        item = new MutableLiveData<>();
        allImages = new MutableLiveData<>();
    }

    public void setItem(String id){
        repo.getItem(id);
        item = repo.getSelectedItem();
    }

    public void setAllImages(String id) {
        repo.getAllImages(id);
        allImages = repo.getAllImages();
    }

    LiveData<List<String>> getAllImages() {return allImages;}

    LiveData<Item> getItem() {
        return item;
    }

    String getItemId() {return item.getValue().getId();}
}
