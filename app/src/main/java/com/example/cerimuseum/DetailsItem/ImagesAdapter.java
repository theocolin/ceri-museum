package com.example.cerimuseum.DetailsItem;

import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cerimuseum.R;

import java.util.List;


public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    private DetailsViewModel viewModel;
    private List<String> imageListWithDescription;

    @Override
    public ImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.image_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String[] picture = imageListWithDescription.get(i).split("[?]");
        String itemId = viewModel.getItemId();
        if(picture.length > 1) {
            viewHolder.description.setText(picture[1]);
        }
        else viewHolder.description.setText("");

        Glide.with(viewHolder.image.getContext())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + itemId + "/images/" + picture[0])
                .into(viewHolder.image);

    }

    @Override
    public int getItemCount() {
        return imageListWithDescription == null ? 0 : imageListWithDescription.size();
    }

    public void setDetailsViewModel(DetailsViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void setListImage(List<String> images) {
        imageListWithDescription = images;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView description;
        ImageView image;

        ActionMode actionMode;

        ViewHolder(View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.description_of_image);
            image = itemView.findViewById(R.id.image_of_item);
        }


    }


}
