package com.example.cerimuseum.data.webservice;

import com.example.cerimuseum.data.item.ItemResponse;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface CWSInterface {
    @Headers("Accept: application/geo+json")
    @GET("categories")
    Call<List<String>> getCategories();

    @Headers("Accept: application/geo+json")
    @GET("collection")
    Call<Map<String, ItemResponse>> getCollection();

    @Headers("Accept: application/geo+json")
    @GET("items/{id_items}")
    Call<ItemResponse> getItems(@Path("id_items") String idItem);
}
