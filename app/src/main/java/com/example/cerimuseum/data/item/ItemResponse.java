package com.example.cerimuseum.data.item;

import java.util.List;
import java.util.Map;

public class ItemResponse {
    public final String name = null;
    public final List<String> categories = null;
    public final String description = null;
    public final List<Integer> timeFrame = null;
    public final Integer year = null;
    public final String brand = null;
    public final List<String> technicalDetails = null;
    public final Map<String, String> pictures = null;
    public final Boolean working = null;

}
