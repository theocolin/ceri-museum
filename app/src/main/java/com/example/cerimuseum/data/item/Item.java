package com.example.cerimuseum.data.item;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(tableName = "item_table", indices = {@Index(value = {"name", "brand"},
        unique = true)})
public class Item {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name = "categories")
    private String categories;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "timeFrame")
    private String timeFrame;

    @ColumnInfo(name = "year")
    private Integer year;

    @ColumnInfo(name = "brand")
    private String brand;

    @ColumnInfo(name = "technicalDetails")
    private String technicalDetails;

    @ColumnInfo(name = "pictures")
    private String pictures;

    @ColumnInfo(name = "working")
    private Boolean working;

    public Item(String name, String categories, String description, String timeFrame, String brand, String technicalDetails, String pictures) {
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.pictures = pictures;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        if(categories != null) {
            if(categories.size() > 1) {
                for(String category : categories) {
                    if(category.equals(categories.get(categories.size() - 1))) {
                        this.categories = this.categories.concat(category);
                    }
                    else this.categories = this.categories.concat(category + " / ");
                }
            }
            else this.categories = categories.get(0);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(List<Integer> timeFrames) {
        if(timeFrames != null) {
            if(timeFrames.size() > 1) {
                for(Integer timeFrame : timeFrames) {
                    if(timeFrame.equals(timeFrames.get(timeFrames.size() - 1))) {
                        this.timeFrame = this.timeFrame.concat(timeFrame.toString());
                    }
                    else this.timeFrame = this.timeFrame.concat(timeFrame.toString() + ";");
                }
            }
            else this.timeFrame = timeFrames.get(0).toString();
        }
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        if (year == null)
        {
            this.year = 0;
        }
        else this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        if(technicalDetails != null) {
            for (String technicalDetail : technicalDetails) {
                this.technicalDetails = this.technicalDetails.concat(technicalDetail + ";");
            }
        }
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(Map<String, String> pictures) {
        if(pictures != null) {
            for(Map.Entry<String, String> entry : pictures.entrySet()) {
                String K = entry.getKey();
                String V = entry.getValue();
                this.pictures = this.pictures.concat(K + "?" + V + ";");
            }
        }
    }

    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
