package com.example.cerimuseum.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.cerimuseum.data.categories.CategoriesResult;
import com.example.cerimuseum.data.database.ItemDAO;
import com.example.cerimuseum.data.database.ItemRoomDatabase;
import com.example.cerimuseum.data.item.Item;
import com.example.cerimuseum.data.item.ItemResponse;
import com.example.cerimuseum.data.item.ItemResult;
import com.example.cerimuseum.data.webservice.CWSInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static com.example.cerimuseum.data.database.ItemRoomDatabase.databaseWriteExecutor;

public class MuseumRepository {
    private static volatile MuseumRepository INSTANCE;
    private final CWSInterface api;

    private LiveData<List<Item>> allItems;
    private final MutableLiveData<Item> selectedItem;

    private volatile MutableLiveData<Boolean> isLoading;
    private final MutableLiveData<Boolean> sortBy;

    private final MutableLiveData<Throwable> webServiceThrowable;

    private final ItemDAO itemDao;

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }

    public MuseumRepository(Application application) {
        ItemRoomDatabase db = ItemRoomDatabase.getDatabase(application);
        itemDao = db.itemDAO();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CWSInterface.class);
        webServiceThrowable = new MutableLiveData<>();
        selectedItem = new MutableLiveData<>();
        allImages = new MutableLiveData<>();
        sortBy = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        //allItems = itemDao.getAllItemsSortName();
        setSortBy(false);

        //if(allItems.getValue() == null) {
        //    loadAllItems();
        //    allItems = itemDao.getAllItemsSortName();
        //    setSortBy(false);
        //}
    }

    public MutableLiveData<Boolean> getIsLoading() {return isLoading;}

    public MutableLiveData<Boolean> getSortBy() {return sortBy;}

    public void setSortBy(Boolean test) {
        this.sortBy.postValue(test);
    }

    public void updateItem(Item item) {
        databaseWriteExecutor.execute(() -> {
            itemDao.updateItem(item);
        });
    }

    public void insertItem(Item item) {
        databaseWriteExecutor.execute(() -> {
            itemDao.insertItem(item);
        });
    }

    public void getItem(String id)  {
        Future<Item> fitem = databaseWriteExecutor.submit(() -> {
            //Log.d("TAG", selectedItem id="+id);
            return itemDao.getItemById(id);
        });
        try {
            selectedItem.setValue(fitem.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private MutableLiveData<List<String>> allImages;
    public void getAllImages(String i) {
        Future<List<String>> fimageDescription = databaseWriteExecutor.submit(() -> {
            String[] test = itemDao.getPictures(i).split("[;]");
            List<String> testl = new ArrayList<>();
            for(String t : test) {
                testl.add(t);
            }
            Log.e("test", testl.toString());
            return testl;
        });
        try {
            allImages.setValue(fimageDescription.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    public LiveData<List<Item>> getAllItemsSortByYear() {

        allItems = itemDao.getAllItemsSortYear();
        return allItems;
    }

    public MutableLiveData<List<String>> getAllImages() {return allImages;}

    public MutableLiveData<Item> getSelectedItem() {return selectedItem;}

    public LiveData<List<Item>> getAllItemsSortByName() {
        allItems = itemDao.getAllItemsSortName();
        return allItems;
    }

    public void loadAllItems() {
        isLoading.postValue(true);
        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call, Response<Map<String, ItemResponse>> response) {
                        // Récupération des items avec leur ID
                        for(String key : response.body().keySet()) {
                            Item test = new Item("", "", "", "", "", "", "");
                            ItemResult.transferInfo(response.body().get(key), test, key);
                            insertItem(test);
                            updateItem(test);
                        }
                        isLoading.postValue(false);
                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        Log.d("ONFAILURE", "Web service erreur");
                        webServiceThrowable.postValue(t);
                        isLoading.postValue(false);
                    }
                }
        );

    }

    public LiveData<CategoriesResult> loadAllCategories() {
        final MutableLiveData<CategoriesResult> result = new MutableLiveData<>();

        result.setValue(new CategoriesResult(null));

        api.getCategories().enqueue(
                new Callback<List<String>>() {
                    @Override
                    public void onResponse(Call<List<String>> call,
                                           Response<List<String>> response) {
                        result.postValue(new CategoriesResult(response.body()));
                    }

                    @Override
                    public void onFailure(Call<List<String>> call, Throwable t) {
                        result.postValue(new CategoriesResult(null));
                    }
                });

        return result;
    }
}
