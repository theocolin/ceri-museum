package com.example.cerimuseum.data.categories;

import java.util.List;

public class CategoriesResult {

    public final List<String> categories;

    public CategoriesResult(List<String> category) {
        this.categories = category;
    }

}

