package com.example.cerimuseum.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.cerimuseum.data.item.Item;

import java.util.List;

@Dao
public interface ItemDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertItem(Item item);

    @Query("DELETE FROM item_table")
    void deleteAllItems();

    @Query("SELECT * from item_table ORDER BY name ASC")
    LiveData<List<Item>> getAllItemsSortName();

    @Query("SELECT * from item_table ORDER BY year ASC")
    LiveData<List<Item>> getAllItemsSortYear();

    @Query("SELECT pictures FROM item_table WHERE _id = :id")
    String getPictures(String id);

    @Query("SELECT * from item_table ORDER BY name ASC")
    List<Item> getSynchrAllItems();

    @Query("DELETE FROM item_table WHERE _id = :id")
    void deleteItem(String id);

    @Query("SELECT * FROM item_table WHERE _id = :id")
    Item getItemById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateItem(Item item);

}
