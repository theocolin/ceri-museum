package com.example.cerimuseum.data.item;

import java.util.HashMap;
import java.util.List;

public class ItemResult {

    public static void transferInfo(ItemResponse itemResponse, Item item, String id) {
        item.setName(itemResponse.name);
        item.setBrand(itemResponse.brand);
        item.setCategories(itemResponse.categories);
        item.setDescription(itemResponse.description);
        item.setTechnicalDetails(itemResponse.technicalDetails);
        item.setTimeFrame(itemResponse.timeFrame);
        item.setWorking(itemResponse.working);
        item.setPictures(itemResponse.pictures);
        item.setYear(itemResponse.year);
        item.setId(id);
    }
}
