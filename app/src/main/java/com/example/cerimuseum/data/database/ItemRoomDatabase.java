package com.example.cerimuseum.data.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.cerimuseum.data.item.Item;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Item.class}, version = 1, exportSchema = false)
public abstract class ItemRoomDatabase extends RoomDatabase {
    private static final String TAG = ItemRoomDatabase.class.getSimpleName();

    public abstract ItemDAO itemDAO();

    private static ItemRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static ItemRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ItemRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ItemRoomDatabase.class,
                                    "item_database")
                                    .build();
                }
            }
        }
        return INSTANCE;
    }
}
