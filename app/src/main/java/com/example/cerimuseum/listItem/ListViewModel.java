package com.example.cerimuseum.listItem;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.cerimuseum.data.MuseumRepository;
import com.example.cerimuseum.data.item.Item;

import java.util.List;

public class ListViewModel extends AndroidViewModel {
    private MuseumRepository repo;

    private final MutableLiveData<Boolean> sortBy;
    private volatile MutableLiveData<Boolean> isLoading;

    private LiveData<List<Item>> allItems;

    public ListViewModel(@NonNull Application application) {
        super(application);
        repo = MuseumRepository.get(application);
        allItems = repo.getAllItemsSortByName();
        isLoading = new MutableLiveData<>();
        sortBy = repo.getSortBy();
    }

    public LiveData<List<Item>> getAllItemsSortByYear() {

        allItems = repo.getAllItemsSortByYear();
        return allItems;
    }

    public LiveData<List<Item>> getAllItemsSortByName() {
        allItems = repo.getAllItemsSortByName();
        return allItems;
    }

    public void setIsLoading() {
        isLoading = repo.getIsLoading();
    }

    MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void updateAllItems() {
        isLoading.setValue(true);
        Thread t = new Thread() {
            public void run() {
                repo.loadAllItems();
            }
        };
        t.start();
    }

    public MutableLiveData<Boolean> getSortBy() {
        return sortBy;
    }
    public void setSortBy(Boolean value) {
        sortBy.postValue(value);
    }

}
