package com.example.cerimuseum.listItem;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cerimuseum.R;

public class ListFragment extends Fragment {

    private ListViewModel viewModel;

    private ItemAdapter adapter;
    private Switch sortItem;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ProgressBar progress;
    private ActionBar mActionBar;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        mActionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        viewModel.setIsLoading();

        listenerSetup();
        observerSetup();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)searchItem.getActionView();

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ItemAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setListViewModel(viewModel);

        sortItem = getView().findViewById(R.id.sort_button);

        mActionBar.setDisplayHomeAsUpEnabled(false);

        //sortItem.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //    }
        //});
        sortItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                viewModel.setSortBy(isChecked);

            }
        });
        progress = getView().findViewById(R.id.progressList);

    }

    private void observerSetup() {
        viewModel.getSortBy().observe(getViewLifecycleOwner(),
                sort -> {
                    if(sort != null) {
                        if(sort) {
                            viewModel.getAllItemsSortByYear().observe(getViewLifecycleOwner(),
                                    items -> {
                                        Log.d("TAG", "Get all items sorted by year");

                                        adapter.setListItem(items);
                                    });
                        }
                        else {
                            viewModel.getAllItemsSortByName().observe(getViewLifecycleOwner(),
                                    items -> {
                                        Log.d("TAG", "Get all items sorted by name");

                                        adapter.setListItem(items);
                                    });
                        }
                    }
                });

        viewModel.getIsLoading().observe(getViewLifecycleOwner(), state -> {
            Log.e("TestAll", state.toString());
            if(state) {
                Log.d("TAG", "progress visibility true");
                progress.setVisibility(View.VISIBLE);
            }
            else {
                Log.d("TAG", "progress visibility false");
                progress.setVisibility(View.GONE);
            }
        });

    }
}