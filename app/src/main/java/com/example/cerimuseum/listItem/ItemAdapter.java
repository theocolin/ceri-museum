package com.example.cerimuseum.listItem;

import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cerimuseum.listItem.ListFragmentDirections;
import com.example.cerimuseum.R;
import com.example.cerimuseum.data.item.Item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> implements Filterable {

    private static final String TAG = ItemAdapter.class.getSimpleName();

    private List<Item> itemsList;
    private List<Item> itemsListFull;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = null;
        switch (i) {
            case 0 :
                v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout_pair_item, viewGroup, false);
                break;
            case 1 :
                v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout_impair_item, viewGroup, false);
                break;
        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if(itemsList.get(i).getName().equals("")) {
            viewHolder.itemName.setText(itemsList.get(i).getBrand());
        }
        viewHolder.itemName.setText(itemsList.get(i).getName());
        viewHolder.itemCategories.setText(itemsList.get(i).getCategories());

        Glide.with(viewHolder.itemMiniature.getContext())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+ itemsList.get(i).getId() +"/thumbnail")
                .into(viewHolder.itemMiniature);
    }

    @Override
    public int getItemViewType(int position) {
        int i = position % 2;
        if (i == 0) {
            return 0;
        }
        return 1;
    }

    @Override
    public Filter getFilter() {
        return itemFilter;
    }
    private final Filter itemFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Item> filteredList = new ArrayList<>();
            if(constraint == null || constraint.length() == 0) {
                filteredList.addAll(itemsListFull);
            }
            else {
                String pattern = constraint.toString().toLowerCase().trim();
                for(Item item : itemsListFull) {
                    if(item.getName().toLowerCase().contains(pattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            itemsList.clear();
            itemsList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    @Override
    public int getItemCount() {
        return itemsList == null ? 0 : itemsList.size();
    }

    public void setListItem(List<Item> items) {
        itemsList = items;
        itemsListFull = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    public void setListViewModel(ListViewModel viewModel) {
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName;
        TextView itemCategories;
        TextView itemBrand;
        ImageView itemMiniature;

        ActionMode actionMode;

        ViewHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_name);
            itemCategories = itemView.findViewById(R.id.item_categories);
            itemBrand = itemView.findViewById(R.id.item_brand);
            itemMiniature = itemView.findViewById(R.id.item_image);

            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.menu_main, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    return false;
                }

                // Called when the user selects a contextual menu item


                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = ItemAdapter.this.itemsList.get((int)getAdapterPosition()).getId();
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action
                            = ListFragmentDirections.actionFirstFragmentToSecondFragment(id);
                    action.setItemId(id);
                    Navigation.findNavController(v).navigate(action);
                }
            });
        }


    }
}
